import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";

class Index extends React.Component {
  constructor(props){
    super(props);

  }

  componentDidMount(){

  }

  render(){
    const { title, data } = this.props;
    return (
      <Row id="#">
        <Col className="section">
          <h5 className="title">{title}</h5>
        </Col>
      </Row>
    );
  }
}

export default Index;
