import React from 'react';
import { Row, Col, Container, Navbar, Nav, Image, NavDropdown } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";

class Index extends React.Component {
  render(){
    var { data } = this.props;
    const linkHtml = data.map((link, i) => {
      var linkStr = "#" + (link.split(" ")[0]).toLowerCase();
      return (
        <Nav.Item><Nav.Link href={linkStr}>{link}</Nav.Link></Nav.Item>
      )
    });
    var linkStr = "#" + data[0];
    return (
      <Navbar collapseOnSelect bg="dark" variant="dark" expand="lg">
        <Navbar.Brand href="#home" className="cwhite">Report</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="navigation-bar">
          <Nav className="justify-content-end" defaultActiveKey={linkStr} variant="pills">
            {linkHtml}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default Index;
