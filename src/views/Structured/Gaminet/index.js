import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";
import { Gallery } from '../../../components';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    return (
      <section id="explainable">
        <Row>
          <Col md={12}>
            <h5 className="title">{title}</h5>
          </Col>
        </Row>
        <p>{data.one_liner}</p>
        <Gallery data={data[keys[1]]} path={path} />
        <br /><br />
        <Gallery data={data[keys[2]]} path={path} />
      </section>
    );
  }
}

export default Index;
