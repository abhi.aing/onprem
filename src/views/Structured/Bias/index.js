import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";
import Attack from './Attack';

class Index extends React.Component {
  constructor(props){
    super(props);

  }

  componentDidMount(){

  }

  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    const table1 = data[keys[1]];
    const table2 = data[keys[2]];
    const table1Data = table1[Object.keys(table1)[1]];
    const table2Data = table2[Object.keys(table2)[1]];
    return (
      <section id="bias">
        <Row>
          <Col md={12}>
            <h5 className="title">{title}</h5>
          </Col>
        </Row>
        <ul>
          <li>
            <Row>
              <Col md={12}>
                <Attack title={keys[0]} data={data[keys[0]]} path={path} />
              </Col>
            </Row>
          </li>
          <li>
            <Row>
              <Col md={12}>
                <h6 className="subtitle">{keys[1]}</h6>
                <p>{data[keys[1]].one_liner}</p>
                <Table striped bordered hover responsive size="sm" variant="dark">
                  <thead>
                    <tr>
                      {Object.keys(table1Data[0]).map((rowKey, i) => <th>{rowKey}</th>)}
                    </tr>
                  </thead>
                  <tbody>
                      {
                        table1Data.map((rowData, i) =>
                          <tr>{Object.keys(rowData).map((row, j) => <td>{rowData[row]}</td>)}</tr>)
                      }
                  </tbody>
                </Table>
              </Col>
            </Row>
          </li>
          <li>
            <Row>
              <Col md={12}>
                <h6 className="subtitle">{keys[2]}</h6>
                <p>{data[keys[2]].one_liner}</p>
                <Table striped bordered hover responsive size="sm" variant="dark">
                  <thead>
                    <tr>
                      {Object.keys(table2Data[0]).map((rowKey, i) => <th>{rowKey}</th>)}
                    </tr>
                  </thead>
                  <tbody>
                    {
                      table2Data.map((rowData, i) =>
                        <tr>{Object.keys(rowData).map((row, j) => <td>{rowData[row]}</td>)}</tr>)
                    }
                  </tbody>
                </Table>
              </Col>
            </Row>
          </li>
        </ul>
      </section>
    );
  }
}

export default Index;
