import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";
import Gallery from 'react-grid-gallery';

class Index extends React.Component {
  constructor(props){
    super(props);

  }

  componentDidMount(){

  }

  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    var IMAGES = [{
      src: path + data[keys[2]],
      thumbnail: path + data[keys[2]],
      caption: keys[2],
      thumbnailWidth: 500,
      thumbnailHeight: 250
    }];
    var tableData = data[keys[1]];
    var cols = Object.keys(tableData);
    var rows = Object.keys(tableData[cols[0]]);
    return (
      <section id="art">
        <Row>
          <Col md={12}>
            <h5 className="title">{title}</h5>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <p>{data.one_liner}</p>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Table striped bordered hover responsive size="lg" variant="dark">
              <thead>
                <tr>
                  {cols.map((col, i) => <th>{col}</th>)}
                </tr>
              </thead>
              <tbody>
                {rows.map((row, i) => <tr>{cols.map((col, j) => <td>{tableData[col][row]}</td>)}</tr>)}
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row>
          <Col md={12} className="section">
            <Gallery images={IMAGES} backdropClosesModal={true} />
          </Col>
        </Row>
      </section>
    );
  }
}

export default Index;
