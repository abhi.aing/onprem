import React from 'react';
import { Row, Col, Container, Form, Button } from 'react-bootstrap';
import  { Images } from '../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import {
  Header, Footer
} from '../../../components';
import Parse from '../../../components/ParseService';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username: "",
      errorMessage: ""
    }
  }

  componentWillMount(){


  }

  reset = (event) => {
    event.preventDefault();
    const { username } = this.state;
    Parse.User.requestPasswordReset(username).then(() => {
      console.log("password reset request sent");
    }).catch((error) => {
      this.setState({
        errorMessage: error.message
      });
    });
  }

  render(){
    const { errorMessage } = this.state;
    const percentage = 66;
    return (
      <Container>
        <Header />
        <Row>
          <Col md={{span:6, offset: 3}}>
            <Form onSubmit={this.reset} className="formBackground">
              <Form.Group className="form-header">
                <h2 className="form-header">Reset Password</h2>
              </Form.Group>
              <hr /><br />
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  required
                  onChange={(event) => this.setState({ username: event.target.value })} />
              </Form.Group>
              <Form.Group controlId="errorMessage">
                <Form.Label className="text-danger">{errorMessage}</Form.Label>
              </Form.Group>
              <Button variant="primary" type="submit" className="myButton" block>
              Reset Password
              </Button>
            </Form>
          </Col>
        </Row>
        <br /><br />
        <Footer />
      </Container>

    )
  }
}

/*
<CircularProgressbar
  value={percentage}
  text={`${percentage}%`}
  styles={buildStyles({
    // Rotation of path and trail, in number of turns (0-1)
    rotation: 0.25,

    // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
    strokeLinecap: 'round',

    // Text size
    textSize: '14px',

    // How long animation takes to go from one percentage to another, in seconds
    pathTransitionDuration: 0.5,

    // Can specify path transition in more detail, or remove it entirely
    // pathTransition: 'none',

    // Colors
    pathColor: `rgba(62, 152, 199, ${percentage / 100})`,
    textColor: '#f88',
    trailColor: '#d6d6d6',
    backgroundColor: '#3e98c7',
  })} />
  */
export default Index;
