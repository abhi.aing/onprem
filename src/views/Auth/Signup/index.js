import React from 'react';
import { Row, Col, Container, Form, Button } from 'react-bootstrap';
import  { Images } from '../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import {
  Header, Footer
} from '../../../components';
import Parse from '../../../components/ParseService';
var disallowedEmailList = require('./free_email_provider_domains');

console.log(disallowedEmailList);

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username: "",
      password: "",
      errorMessage: ""
    }
  }

  componentWillMount(){

  }

  componentDidMount() {
    const currentUser = Parse.User.current();
    if(currentUser){
      Parse.User.logOut();
    }
  }

  signup = (event) => {
    event.preventDefault();
    const { username, password, confirm } = this.state;
    if(password != confirm){
      this.setState({
        errorMessage: "Passwords do not match"
      });
    } /*else if(username.contains("@gmail.com") || username.contains())) {

    }*/
    else {
      const user = new Parse.User();
      console.log(username + " "+ password + " " + confirm);
      user.set("username", username);
      user.set("password", password);
      user.set("email", username);
      user.signUp().then(() => {
        console.log("signup complete");
      }).catch((error) => {
        this.setState({
          errorMessage: error.message
        });
      })
    }
  }

  render(){
    const { errorMessage } = this.state;
    const percentage = 66;
    return (
      <Container>
        <Header />
        <Row>
          <Col md={{span:6, offset:3}}>
            <Form onSubmit={this.signup} className="formBackground">
              <Form.Group className="form-header">
                <h2 className="form-header">Signup</h2>
              </Form.Group>
              <hr /><br />
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  required
                  type="email"
                  placeholder="Enter email"
                  onChange={(event) => this.setState({username: event.target.value})} />
                <Form.Text className="text-muted">
                  Kindly use your organization email.
                </Form.Text>
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  required
                  type="password"
                  placeholder="Password"
                  onChange={(event) => this.setState({password: event.target.value})} />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                  required
                  type="text"
                  placeholder="Password"
                  onChange={(event) => this.setState({confirm: event.target.value, errorMessage: ""})} />
              </Form.Group>
              <Form.Group controlId="ErrorControl">
                <Form.Label className="text-danger">{errorMessage}</Form.Label>
              </Form.Group>
              <Button variant="primary" type="submit" block className="myButton">
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
        <Footer />
      </Container>
    )
  }
}

export default Index;
