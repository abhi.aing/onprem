import React from 'react';
import { Row, Col, Container, Form } from 'react-bootstrap';
import  { Images } from '../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }

  componentWillMount(){

  }

  render(){
    const { } = this.state;
    const percentage = 66;
    return (
      <Container>
        <Row>
          <Col md={{span:4, offset: 4}}>
            <br />
            <CircularProgressbar
              value={percentage}
              text={`${percentage}%`}
              styles={buildStyles({
                // Rotation of path and trail, in number of turns (0-1)
                rotation: 0.25,

                // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                strokeLinecap: 'round',

                // Text size
                textSize: '14px',

                // How long animation takes to go from one percentage to another, in seconds
                pathTransitionDuration: 0.5,

                // Can specify path transition in more detail, or remove it entirely
                // pathTransition: 'none',

                // Colors
                pathColor: `rgba(62, 152, 199, ${percentage / 100})`,
                textColor: '#f88',
                trailColor: '#d6d6d6',
                backgroundColor: '#3e98c7',
              })} />
              <Form className="main">

              </Form>
          </Col>
        </Row>
      </Container>

    )
  }
}

export default Index;
