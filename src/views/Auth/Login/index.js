import React from 'react';
import { Row, Col, Container, Form, Button } from 'react-bootstrap';
import  { Images } from '../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";
import { Header, Footer } from '../../../components';
import Parse from '../../../components/ParseService';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username: "",
      password: "",
      errorMessage: ""
    }
  }

  componentWillMount(){

  }

  login = (e) => {
    e.preventDefault();
    const { username, password } = this.state;
    Parse.User.logIn(username, password).then(() => {
      console.log("User logged In");
      var currentUser = Parse.User.current();
      if(currentUser){
        //Parse.User.logOut();
        console.log(currentUser.getUsername());
      }
    }).catch((error) => {
      this.setState({
        errorMessage: error.message
      });
    });
  }

  render(){
    const { username, password, errorMessage } = this.state;
    const percentage = 66;
    return (
      <Container>
        <Row>
          <Header />
          <Col md={{span:6, offset: 3}}>
              <br />
              <Form onSubmit={this.login} className="formBackground">
                <Form.Group className="form-header">
                  <h2 className="form-header">Login</h2>
                </Form.Group>
                <hr /><br />
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email Address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter Email"
                    value={username}
                    onChange={(event) => this.setState({ username: event.target.value })}
                    />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(event) => this.setState({ password: event.target.value })} />
                </Form.Group>
                <Form.Group controlId="errorMessage">
                  <Form.Label className="text-danger">{errorMessage}</Form.Label>
                </Form.Group>
                <Button variant="primary" type="submit" className="myButton" block>
                Submit
                </Button>
                <br />
                <Form.Row>
                  <Col md={6} align="left">
                    <Form.Label><Link to="/auth/signup" className="myLink">New Registration ?</Link></Form.Label>
                  </Col>
                  <Col md={6} align="right">
                    <Form.Label><Link to="/auth/forget" className="myLink">Forgot Password ?</Link></Form.Label>
                  </Col>
                </Form.Row>
              </Form>
          </Col>
        </Row>
        <Footer />
      </Container>

    )
  }
}

export default Index;
