import React from 'react';
import { Row, Col, Container, Navbar, Nav, Image, NavDropdown } from 'react-bootstrap';
import  { Images } from '../../themes';
import "./index.css";
import {
  Header, Footer
} from '../../components';
import Form from './Form';
import Signup from './Signup';
import Login from './Login';
import Forget from './Forget';
import Profile from './Profile';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  withRouter
} from "react-router-dom";

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }

  componentWillMount(){

  }
  render(){
    const { } = this.state;
    //let { path, url } = useRouteMatch();
    console.log(this.props);
    return (
      <Container className="main">
        <Header />
        <Switch>
          <Route path={this.props.match.path}>
            <Login />
          </Route>
          <Route path="/signup">
            <Signup />
          </Route>
        </Switch>
        <Footer />
      </Container>
    )
  }
}

export default withRouter(Index);
