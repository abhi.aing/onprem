import React from 'react';
import { Row, Col, Container, Navbar, Nav, Image, NavDropdown, Button, Form } from 'react-bootstrap';
import  { Images } from '../../themes';
import "./index.css";
import {
  Header, Footer
} from '../../components';
import Imagepipeline from './Imagepipeline';
import Structured from './Structured';
import Textpipeline from './Textpipeline';
import Regression from './Regression';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight, faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      option: 0
    }
  }

  componentWillMount(){

  }

  selectOption = (e) => {
    //conso
  }

  render(){
    const { option } = this.state;
    var config = "";
    if(option == 0){
      config = <Imagepipeline />;
    } else if(option == 1){
      config = <Regression />;
    } else if(option == 2){
      config = <Structured />;
    } else if(option == 3){
      config = <Textpipeline />;
    }
    return (
      <Container className="main">
        <Header title="Customize Report" showReportSelector={false} />
        <Row>
          <Col md={{span:12}}>
            <br />
            <Form.Group as={Col} controlId="formGridState">
              <Form.Row>
                <Col md={8} className="selectOption">
                  <Form.Control as="select" defaultValue="Choose...">
                    <option>Image Pipeline</option>
                    <option>Regression</option>
                    <option>Structured</option>
                    <option>Text Pipeline</option>
                  </Form.Control>
                </Col>
                <Col md={{span: 4}}>
                  <Button variant="primary" block size="lg" className="myButton">
                    Launch <FontAwesomeIcon icon={faArrowCircleRight} />
                  </Button>
                </Col>
              </Form.Row>
            </Form.Group>
            {config}
          </Col>
        </Row>
        <Footer />
      </Container>
    )
  }
}

export default Index;
