import React from 'react';
import { Row, Col, Container, Form, Accordion, Card, Button, Tabs, Tab, TabContainer } from 'react-bootstrap';
import  { Images } from '../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import RangeSlider from 'react-bootstrap-range-slider';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight, faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
    }
  }

  componentWillMount(){

  }

  render(){
    const { title, keyValue } = this.props;
    console.log(keyValue);
    return (
      <Card.Header>
        <Accordion.Toggle as={Button} variant="link" eventKey="0" block>
        <Row>
          <Col md={8} align="left">
            {title}
          </Col>
          <Col md={4} align="right">
            <FontAwesomeIcon icon={faArrowCircleDown} />
          </Col>
        </Row>
        </Accordion.Toggle>
      </Card.Header>
    )
  }
}

export default Index;
