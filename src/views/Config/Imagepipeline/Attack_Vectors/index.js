import React from 'react';
import { Row, Col, Container, Form, Accordion, Card, Button, Tabs, Tab, TabContainer } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import RangeSlider from 'react-bootstrap-range-slider';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight, faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      samples: 12
    }
  }

  componentWillMount(){

  }

  render(){
    const { samples } = this.state;
    return (
      <Card.Body>
        <Row>
          <Col md={6}>
            <h5>Samples</h5>
            <RangeSlider
              value={samples}
              max={32}
              max={128}
              step={2}
              tooltip={'on'}
              tooltipPlacement={'top'}
              onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
          </Col>
          <Col md={6}>
            <h5>Categorization of Attacks</h5>
            <Form.Control as="select" size="sm" custom>
              <option>Low Threat & Fast Execution</option>
              <option>Moderate</option>
              <option>Exhaustive</option>
            </Form.Control>
          </Col>
        </Row>
        <TabContainer>
          <Tabs defaultActiveKey="10" id="technique">
            <Tab eventKey="0" title="FGSM">
              <Row>
                <Col md={6}>
                  <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
                </Col>
                <Col md={6}>
                  <RangeSlider
                    value={samples}
                    max={32}
                    max={128}
                    step={2}
                    tooltip={'on'}
                    onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
                </Col>
              </Row>
            </Tab>
            <Tab eventKey="1" title="IFS">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="2" title="UP">
              <Row>
                <Col md={3}>
                  <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
                </Col>
                <Col md={6}>
                  <RangeSlider
                    value={samples}
                    max={32}
                    max={128}
                    step={2}
                    tooltip={'on'}
                    onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
                </Col>
                <Col md={3}>
                  <Form.Check inline label="Targeted" type={'switch'}  id={'check2'} />
                </Col>
              </Row>
            </Tab>
            <Tab eventKey="3" title="BIM">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="4" title="JSM">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="5" title="DF">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="6" title="EN">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="7" title="NF">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="8" title="Clinf">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="9" title="HSJ">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="10" title="PA">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="11" title="ZOO">
              <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
            </Tab>
            <Tab eventKey="12" title="ST">
              <br />
              <br />
              <Row>
                <Col md={2}>
                  <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
                </Col>
                <Col md={2}>
                  <RangeSlider
                    value={samples}
                    max={32}
                    max={128}
                    step={2}
                    tooltip={'on'}
                    tooltipPlacement={'top'}
                    onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
                </Col>
                <Col md={2}>
                  <RangeSlider
                    value={samples}
                    max={32}
                    max={128}
                    step={2}
                    tooltip={'on'}
                    tooltipPlacement={'top'}
                    onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
                </Col>
                <Col md={2}>
                  <RangeSlider
                    value={samples}
                    max={32}
                    max={128}
                    step={2}
                    tooltip={'on'}
                    tooltipPlacement={'top'}
                    onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
                </Col>
                <Col md={2}>
                  <RangeSlider
                    value={samples}
                    max={32}
                    max={128}
                    step={2}
                    tooltip={'on'}
                    tooltipPlacement={'top'}
                    onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
                </Col>
              </Row>
            </Tab>
            <Tab eventKey="13" title="DBB">
              <Row>
                <Col md={2}>
                  <Form.Check inline label="Set Execution" type={'switch'}  id={'check1'} />
                </Col>
                <Col md={4}>
                  <Form.Label>Delta</Form.Label>
                  <RangeSlider
                    value={samples}
                    max={32}
                    max={128}
                    step={2}
                    tooltip={'on'}
                    tooltipPlacement={'top'}
                    onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
                </Col>
                <Col md={4}>
                  <Form.Label>Epsilon</Form.Label>
                  <RangeSlider
                    value={samples}
                    max={32}
                    max={128}
                    step={2}
                    tooltip={'on'}
                    tooltipPlacement={'top'}
                    onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
                </Col>
                <Col md={2}>
                  <Form.Check inline label="Targeted" type={'switch'} id={'check1'} />
                </Col>
              </Row>
            </Tab>
          </Tabs>
        </TabContainer>
      </Card.Body>
    )
  }
}

export default Index;
