import React from 'react';
import { Row, Col, Container, Form, Accordion, Card, Button, Tabs, Tab, TabContainer } from 'react-bootstrap';
import  { Images } from '../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import RangeSlider from 'react-bootstrap-range-slider';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight, faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';
import Header from '../Header';
import Loading from './Loading';
import Modeling from './Modeling';
import Explainability from './Explainability';
import Metamorphic from './Metamorphic';
import Attack_Vectors from './Attack_Vectors';
import Deepxplore from './Deepxplore';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      coverage: 0.75
    }
  }

  componentWillMount(){

  }

  render(){
    return (
      <Accordion>
        <Card>
          <Header title="Loading Config" keyValue={0} />
          <hr className="break"/>
          <Accordion.Collapse eventKey="0">
            <Loading />
          </Accordion.Collapse>
        </Card>
        <Card>
          <Header title="Modeling" keyValue={1} />
          <Accordion.Collapse eventKey="1">
            <Modeling />
          </Accordion.Collapse>
        </Card>
        <Card>
          <Header title="Model Explainability" keyValue={2} />
          <Accordion.Collapse eventKey="2">
            <Explainability />
          </Accordion.Collapse>
        </Card>
        <Card>
          <Header title="Attack Vectors" keyValue={3} />
          <Accordion.Collapse eventKey="3">
            <Attack_Vectors />
          </Accordion.Collapse>
        </Card>
        <Card>
          <Header title="Metamorphic Testing" keyValue={4} />
          <Accordion.Collapse eventKey="4">
            <Metamorphic />
          </Accordion.Collapse>
        </Card>
        <Card>
          <Header title="Deepxplore" keyValue={5} />
          <Accordion.Collapse eventKey="5">
            <Deepxplore />
          </Accordion.Collapse>
        </Card>
      </Accordion>
    )
  }
}

export default Index;
