import React from 'react';
import { Row, Col, Container, Form, Accordion, Card, Button, Tabs, Tab, TabContainer } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import RangeSlider from 'react-bootstrap-range-slider';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight, faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {


    }
  }

  componentWillMount(){

  }

  render(){
    const {  } = this.state;
    return (
      <Card.Body>
        Transformation<br />
        <Form>
          <Form.Check inline label="Shear" type={'switch'} checked id={'metacheck1'} />
          <Form.Check inline label="Rotation" type={'switch'} checked id={'metacheck2'} />
          <Form.Check inline label="Translation_x" type={'switch'} id={'metacheck3'} />
          <Form.Check inline label="Translation_y" type={'switch'} id={'metacheck4'} />
          <Form.Check inline label="Brightness" type={'switch'} checked id={'metacheck5'} />
          <Form.Check inline label="Contrast" type={'switch'} id={'metacheck6'} />
          <Form.Check inline label="Zoom_xy" type={'switch'} checked id={'metacheck7'} />
          <Form.Check inline label="Blur_avg" type={'switch'} id={'metacheck8'} />
          <Form.Check inline label="txty" type={'switch'} checked id={'metacheck9'} />
          <Form.Check inline label="Individual_Transformations" type={'switch'}  id={'metacheck10'} />
          <br />
          <br />
          [Part THREE]
          <br />
          <Form.Check inline label="execute.part.three.accuracy.consolidation.deviations" type={'switch'}  id={'metacheck11'} />
          <br />
          <br />
          [MODEL]
          <br />
          <Form.Check inline label="Dimensions" type={'switch'}  id={'metacheck11'} />
        </Form>
      </Card.Body>
    )
  }
}

export default Index;
