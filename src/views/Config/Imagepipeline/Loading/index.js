import React from 'react';
import { Row, Col, Container, Form, Accordion, Card, Button, Tabs, Tab, TabContainer } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import RangeSlider from 'react-bootstrap-range-slider';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight, faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      threshold: 0.5,
      height: 100,
      width: 100,
      size: 0.5

    }
  }

  componentWillMount(){

  }

  render(){
    const { threshold, height, width, size } = this.state;
    return (
      <Card.Body>
        <Row>
          <Col md={6}>
            <Form.File id="inputPath" label="Input Path" />
          </Col>
        </Row>
        <br />
        <Form.Label>Remove Duplicate</Form.Label>
        <Row>
          <Col md={6}>
            <Form.Check inline label="Set Execution" type={'switch'} id={'check_rm_dupl'} />
          </Col>
          <Col md={6}>
          </Col>
        </Row>
        <br />
        <Form.Label>Remove Outlier</Form.Label>
        <Row>
          <Col md={6}>
            <Form.Check inline label="Set Execution" type={'switch'} id={'check_rm_outl'} />
          </Col>
          <Col md={6}>
            <Form.Label>Threshold</Form.Label>
            <RangeSlider
              value={threshold}
              min={0}
              max={1}
              step={0.1}
              tooltip={'on'}
              tooltipPlacement={'top'}
              onChange={changeEvent => this.setState({threshold : changeEvent.target.value}) }/>
          </Col>
        </Row>
        <Form.Label>Loading Data</Form.Label>
        <Row>
          <Col md={3}>
            <Form.Check inline label="Set Execution" type={'switch'}  id={'check_load_data'} />
          </Col>
          <Col md={3}>
            <Form.Label>Image Height</Form.Label>
            <RangeSlider
              value={height}
              max={32}
              max={128}
              step={2}
              tooltip={'on'}
              tooltipPlacement={'top'}
              onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
          </Col>
          <Col md={3}>
            <Form.Label>Image Width</Form.Label>
            <RangeSlider
              value={width}
              max={32}
              max={128}
              step={2}
              tooltip={'on'}
              tooltipPlacement={'top'}
              onChange={changeEvent => this.setState({width : changeEvent.target.value}) }/>
          </Col>
          <Col md={3}>
            <Form.Label>Test Size</Form.Label>
            <RangeSlider
              value={size}
              min={0}
              max={1}
              step={0.1}
              tooltip={'on'}
              tooltipPlacement={'top'}
              onChange={changeEvent => this.setState({size : changeEvent.target.value}) }/>
          </Col>
        </Row>
      </Card.Body>
    )
  }
}

export default Index;
