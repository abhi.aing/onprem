import React from 'react';
import { Row, Col, Container, Form, Accordion, Card, Button, Tabs, Tab, TabContainer } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import RangeSlider from 'react-bootstrap-range-slider';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight, faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      epoch: 12,
      samples: 12
    }
  }

  componentWillMount(){

  }

  render(){
    const { epoch, samples } = this.state;
    return (
      <Card.Body>
        <Row>
          <Col md={6}>
            <Form.Label>Epochs</Form.Label>
            <RangeSlider
              value={epoch}
              max={0}
              max={100}
              step={2}
              tooltip={'on'}
              tooltipPlacement={'top'}
              onChange={changeEvent => this.setState({epoch : changeEvent.target.value}) }/>
          </Col>
          <Col md={6}>
            <Form.Label>Batch Size</Form.Label>
            <RangeSlider
              value={samples}
              max={32}
              max={128}
              step={2}
              tooltip={'on'}
              tooltipPlacement={'top'}
              onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
          </Col>
        </Row>
      </Card.Body>
    )
  }
}

export default Index;
