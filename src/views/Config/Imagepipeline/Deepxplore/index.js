import React from 'react';
import { Row, Col, Container, Form, Accordion, Card, Button, Tabs, Tab, TabContainer } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./index.css";
import RangeSlider from 'react-bootstrap-range-slider';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight, faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      coverage: 50,
      samples: 50
    }
  }

  componentWillMount(){

  }

  render(){
    const { coverage, samples } = this.state;
    return (
      <Card.Body>
        <h3>Transformation</h3>
        <Form>
          <Form.Check inline label="Blackout" type={'switch'}  id={'check1'} />
          <Form.Check inline label="Contrast" type={'switch'}  id={'check2'} />
          <Form.Check inline label="Occlusion" type={'switch'}  id={'check3'} />
          <Form.Check inline label="Translate" type={'switch'}  id={'check4'} />
          <Form.Check inline label="Blur" type={'switch'} id={'check5'}  />
          <Form.Check inline label="Shear" type={'switch'} id={'check6'}  />
          <Form.Check inline label="Rotation" type={'switch'} id={'check7'}  />
          <Form.Check inline label="Zoom" type={'switch'} id={'check8'}  />
          <br />
          <br />
          <h3>Parameter</h3>
          <Row>
            <Col md={6}>
              <Form.Label>n_samples</Form.Label>
              <RangeSlider
                value={samples}
                max={32}
                max={128}
                step={2}
                tooltip={'on'}
                onChange={changeEvent => this.setState({samples : changeEvent.target.value}) }/>
            </Col>
            <Col md={6}>
              <Form.Label>n_coverage_threshold</Form.Label>
              <RangeSlider
                value={coverage}
                max={0}
                max={1}
                step={0.01}
                tooltip={'on'}
                onChange={changeEvent => this.setState({coverage : changeEvent.target.value}) }/>
            </Col>
          </Row>
        </Form>
      </Card.Body>
    )
  }
}

export default Index;
