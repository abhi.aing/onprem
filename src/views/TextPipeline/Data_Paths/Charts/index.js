import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import { Doughnut, Line, Bar } from 'react-chartjs-2';

import "./index.css";

class Index extends React.Component {
  constructor(props){
    super(props);

  }

  componentDidMount(){

  }

  render(){
    const { data, title, path } = this.props;
    const keys = Object.keys(data);
    const chart1Data = data[keys[0]];
    const chart2Data = data[keys[1]];
    const chartData={
    	labels: Object.keys(chart1Data),
    	datasets: [{
    		data: Object.keys(chart1Data).map((row) => chart1Data[row]),
    		backgroundColor: [
    		'#FF6384',
    		'#36A2EB',
        '#FF0000'
    		]
    	}]
    };
    const chartData2={
    	labels: Object.keys(chart2Data),
    	datasets: [{
    		data: Object.keys(chart2Data).map((row) => chart2Data[row]),
    		backgroundColor: [
    		'#FF6384',
    		'#36A2EB',
        '#FF0000'
    		]
    	}]
    };
    return (
      <Row>
        <Col md={6}>
          <h5 className="title">{keys[0]}</h5>
          <Doughnut data={chartData} />
        </Col>
        <Col md={6}>
          <h5 className="title">{keys[1]}</h5>
          <Doughnut data={chartData2} />
        </Col>
      </Row>
    )
  }
}

export default Index;
