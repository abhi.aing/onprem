import React from 'react';
import { Row, Col, Container, Nav, Image, Table, Tabs, Tab } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import "./index.css";
import { Gallery } from '../../../../components';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    var table1Html = keys.map((row, i) => {
      return (
        <tr key={i}>
          <td className="text">{data[i].Text}</td>
          <td>{data[i].Target}</td>
        </tr>
      );
    });
    return (
      <Row id="global">
        <Col className="section">
          <h6 className="subtitle">{title}</h6>
          <Table striped bordered hover responsive size="sm" variant="dark">
            <thead>
              <tr>{Object.keys(data[keys[0]]).map((item, i) => <td>{item}</td>)}</tr>
            </thead>
            <tbody>
            {table1Html}
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  }
}

export default Index;
