import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../themes';
import Charts from './Charts';
import MetaPlots from './MetaPlots';
import HeadTextData from './HeadTextData';
import "./index.css";
import Gallery from 'react-grid-gallery';
import Class_Distribution from './Class_Distribution';
import OutlierTable from './OutlierTable';

class Index extends React.Component {
  constructor(props){
    super(props);

  }

  componentDidMount(){

  }

  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    return (
      <section id="data">
        <Row>
          <Col md={12}>
            <h5 className="title">{title}</h5>
          </Col>
        </Row>
        <Class_Distribution data={data[keys[0]]} path={path} title={keys[0]} />
        <Charts data={data[keys[1]]} path={path} />
        <br />
        <OutlierTable data={data[keys[2]]} path={path} title={keys[2]} />
        <HeadTextData data={data[keys[3]]} path={path} title = {keys[3]} />
        <MetaPlots data={data[keys[4]]} path={path} title = {keys[4]} />
      </section>
    );
  }
}

export default Index;
