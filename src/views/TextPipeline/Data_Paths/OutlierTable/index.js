import React from 'react';
import { Row, Col, Container, Nav, Image, Table, Tabs, Tab } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import "./index.css";

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    var table1Html = Object.keys(data).map((rowKey, i) => (
      <tr key={i}>
        <td>{rowKey}</td>
        {Object.keys(data[rowKey]).map((row, i) => <td>{data[rowKey][row]}</td>)}
      </tr>
    ));
    return (
      <Row>
        <Col md={12}>
          <h5 className="subtitle">{title}</h5>
          <Table striped bordered hover responsive size="sm" variant="dark">
            <thead>
              <tr>
                <td>Dataset</td>
                <td>n</td>
                <td>%</td>
              </tr>
            </thead>
            <tbody>
              {table1Html}
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  }
}

export default Index;
