import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import "./index.css";
import { Gallery } from '../../../../../components';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    return (
      <Row>
        <Col className="attack-section">
          <Gallery data={data} path={path} />
        </Col>
      </Row>
    );
  }
}

export default Index;
