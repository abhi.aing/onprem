import React from 'react';
import { Row, Col, Container, Nav, Image, Table, Tabs, Tab } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import "./index.css";
import Gallery from 'react-grid-gallery';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    var IMAGES = [{
      src: path + data.img_path,
      thumbnail: path + data.img_path,
      caption: title,
      thumbnailWidth: 1200,
      thumbnailHeight: 300
    }];
    return (
      <Row>
        <Col md={12} className="section">
          <h5 className="subtitle">{title}</h5>
          <Gallery images={IMAGES} backdropClosesModal={true} />
        </Col>
      </Row>
    );
  }
}

export default Index;
