import React from 'react';
import { Row, Col, Container, Navbar, Nav, Image, NavDropdown } from 'react-bootstrap';
import  { Images } from '../../themes';
import "./index.css";
import Navigation from './Navigation';
import Data_Paths from './Data_Paths';
import Modeling_Pipeline from './Modeling_Pipeline';
import Model_Explainability from './Model_Explainability';
import ScrollToTop from "react-scroll-to-top";
//import { ReactComponent as MySVG } from "../../images/uparrow.svg";
import {
  Header,
  Footer
} from '../../components';
import Dataset_Selector from './Dataset_Selector';
var paths = [
  'data/text/amazon_reviews/'
];

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      choice: 1,
      data: null
    }
  }

  componentWillMount(){
    this.setData(0);
  }

  setData = (choice) => {
    fetch(paths[choice] + "json_metadata.json")
    .then((res) => res.json())
    .then((data) => {
      this.setState({
        data: data,
        choice: choice
      });
    });
  }

  pickChoice = (choice) => {
    this.setData(--choice);
  }

  /*
  <Local_Visualization title={keys[2]} data={data[keys[2]]} path={path} />
  <hr />
  <Data_Paths title={keys[0]} data={data[keys[0]]} path={path} />

  <Dataset_Selector choice={this.pickChoice} />

  */
  render(){
    const { choice, data } = this.state;
    if(data == null) return null;
    var keys = Object.keys(data);
    var path = paths[choice];
    return (
      <Container className="main">
        <Header title="Text Pipeline" />
        <Navigation data={keys} />
        <Modeling_Pipeline title={keys[1]} data={data[keys[1]]} path={path} />
        <hr />
        <Model_Explainability title={keys[2]} data={data[keys[2]]} path={path} />
        <Footer />
        <ScrollToTop smooth color="#A03C64" />
      </Container>
    )
  }
}

export default Index;
