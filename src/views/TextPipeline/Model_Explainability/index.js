import React from 'react';
import { Row, Col, Container, Nav, Image, Table, Tabs, Tab } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";
import Local_Visualization from './Local_Visualization';
import Global_Visualization from './Global_Visualization';
import Fairml from './Fairml';

class Index extends React.Component {
  constructor(props){
    super(props);

  }

  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    return (
      <Row>
        <Col className="section">
          <h6 className="subtitle">{title}</h6>
          <ul>
            <li><Global_Visualization title={keys[0]} data={data[keys[0]]} path={path} /></li>
            <br />
            <li><Local_Visualization title={keys[1]} data={data[keys[1]]} path={path} /></li>
            <br />
            <li><Fairml title={keys[2]} data={data[keys[2]]} path={path} /></li>
          </ul>
        </Col>
      </Row>
    );
  }
}

export default Index;
