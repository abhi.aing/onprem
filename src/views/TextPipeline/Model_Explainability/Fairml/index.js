import React from 'react';
import { Row, Col, Container, Nav, Image, Table, Tabs, Tab } from 'react-bootstrap';
import  { Images } from '../../../../themes';
import "./index.css";
import Attack from './Attack';

class Index extends React.Component {
  constructor(props){
    super(props);

  }

/*


<Tab eventKey={keys[2]} title={keys[2]}>
  <Attack title={keys[2]} data={data[keys[2]]} path={path} />
</Tab>
<Tabs defaultActiveKey={keys[0]}>
  <Tab eventKey={keys[0]} title={keys[0]}>
    <Attack title={keys[0]} data={data[keys[0]]} path={path} />
  </Tab>
  <Tab eventKey={keys[1]} title={keys[1]}>
    <Attack title={keys[1]} data={data[keys[1]]} path={path} />
  </Tab>
</Tabs>
*/
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    return (
      <Row id="fairml">
        <Col className="section">
          <h6 className="subtitle">{title}</h6>
          <Row>
            <Col md={12}>
              <Attack title={keys[0]} data={data[keys[0]]} path={path} />
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default Index;
