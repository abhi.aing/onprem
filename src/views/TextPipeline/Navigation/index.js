import React from 'react';
import { Row, Col, Container, Navbar, Nav, Image, NavDropdown } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";

class Index extends React.Component {
  render(){
    var { data } = this.props;
    const linkHtml = data.map((link, i) => {
      var linkStr = "#" + (link.split(" ")[0]).toLowerCase();
      return (
        <Nav.Item><Nav.Link href={linkStr}>{link}</Nav.Link></Nav.Item>
      )
    });
    var linkStr = "#" + data[0];
    return (
      <Navbar collapseOnSelect bg="dark" variant="dark" expand="lg">
        <Navbar.Brand href="#home" className="cwhite">Report</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="navigation-bar">
          <Nav className="justify-content-end" defaultActiveKey={linkStr} variant="pills">
            <Nav.Item><Nav.Link href="#loading">Data Diagnostic</Nav.Link></Nav.Item>
            <Nav.Item><Nav.Link href="#modeling">Modeling</Nav.Link></Nav.Item>
            <NavDropdown title="Model Explainability">
              <NavDropdown.Item eventKey="3.1" href="#global">Global Explainability</NavDropdown.Item>
              <NavDropdown.Item eventKey="3.2" href="#local">Local Explainability</NavDropdown.Item>
              <NavDropdown.Item eventKey="3.3" href="#fairml">FairML</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default Index;
