import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";
import { Doughnut } from 'react-chartjs-2';
import Gallery from 'react-grid-gallery';

class Index extends React.Component {
  constructor(props){
    super(props);

  }

  componentDidMount(){

  }

  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    var rowKeys = Object.keys(data[keys[2]].duplicate);
    var rowsHtml = rowKeys.map((rowKey, i) => {
      return (
        <tr key={i}>
          <td>{rowKey}</td>
          <td>{data[keys[2]].duplicate[rowKey][1]}</td>
          <td>{data[keys[2]].duplicate[rowKey][0]}</td>
          <td>{data[keys[2]].outlier[rowKey][1]}</td>
          <td>{data[keys[2]].outlier[rowKey][0]}</td>
        </tr>
      );
    });

    var IMAGES = [{
        src: path + data[keys[1]],
        thumbnail: path + data[keys[1]],
        thumbnailWidth: 600,
        thumbnailHeight: 150,
      }];
    const chartData={
    	labels: [
    		'Data Points',
    		'Validation Points'
    	],
    	datasets: [{
    		data: [data[keys[2]].train_shape[0], data[keys[2]].test_shape[0]],
    		backgroundColor: [
    		'#FF6384',
    		'#36A2EB'
    		]
    	}]
    };
    return (
      <section id="loading">
        <Row>
          <Col md={12}>
            <h5 className="title">{title}</h5>
          </Col>
        </Row>
        <Row>
          <Col md={8} className="section">
            <Gallery images={IMAGES} backdropClosesModal={true} />
          </Col>
          <Col md={4}>
            <p>Image Dimension: {data[keys[2]].image_shape[0]} x {data[keys[2]].image_shape[1]}</p>
            <p>Test Size: {data[keys[2]].test_size}</p>
            <Doughnut data={chartData} />
          </Col>
        </Row>
        <br />
        <Row>
          <Col md={12}>
            <Table striped bordered hover responsive size="lg" variant="dark">
              <thead>
                <tr>
                  <th>Classname</th>
                  <th>Duplicate</th>
                  <th>Duplicate %</th>
                  <th>Outlier</th>
                  <th>Outlier %</th>
                </tr>
              </thead>
              <tbody>
                {rowsHtml}
              </tbody>
            </Table>
          </Col>
        </Row>
      </section>
    );
  }
}

export default Index;
