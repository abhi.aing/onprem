 import React from 'react';
import { Row, Col, Container, Nav, Image, Table, Tabs, Tab } from 'react-bootstrap';
import "./index.css";
import Attack from '../Attack';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    var tableHtml = '';
    var keys = Object.keys(data);
    return (
      <Row id="art-whitebox">
        <Col className="section">
          <h6 className="subtitle">{title}</h6>
          <Tabs defaultActiveKey={keys[0]}>
            {keys.map((key, i) => {
              return (
                <Tab eventKey={key} title={key}>
                  <Attack title={key} data={data[keys[i]]} path={path} />
                </Tab>
              );
            })}
          </Tabs>
        </Col>
      </Row>
    );
  }
}

export default Index;
