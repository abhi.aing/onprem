import React from 'react';
import { Row, Col, Container, Nav, Image } from 'react-bootstrap';
import "./index.css";
import Whitebox from './Whitebox';
import Blackbox from './Blackbox';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    return (
      <Row id="#art">
        <Col className="section">
          <h5 className="title">{title}</h5>
          <br />
          <h6>{keys[0]}{data[keys[0]]}</h6>
          <br />
          <ul>
            <li><Whitebox title={keys[1]} data={data[keys[1]]} path={path} /></li>
            <hr />
            <li><Blackbox title={keys[2]} data={data[keys[2]]} path={path} /></li>
          </ul>
        </Col>
      </Row>
    );
  }
}

export default Index;
