import React from 'react';
import { Row, Col, Container, Nav, Image, Table, Tabs, Tab } from 'react-bootstrap';
import Gallery from 'react-grid-gallery';
import  { Images } from '../../../themes';
import Class from './Class';
import "./index.css";

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    const tabsData = data[keys[2]];
    const tableData = data[keys[1]];
    const rows = Object.keys(tableData[0]);
    var rowsHtml = "";
    return (
      <Row id="model_privacy">
        <Col className="section">
          <h5 className="title">{title}</h5>
          <Row>
            <Col md={12}>
              <p>{data.one_liner}</p>
            </Col>
            <Col md={12}>
              <h6 className="subtitle">{keys[1]}</h6>
              <Table striped bordered hover responsive size="lg" variant="dark">
                <thead>
                  <tr>
                    {rows.map((row, i) => <th>{row}</th>)}
                  </tr>
                </thead>
                <tbody>
                  {tableData.map((row,i) => {
                    return <tr>{Object.keys(row).map((col, j) => <td>{row[col]}</td>)}</tr>
                  })}
                </tbody>
              </Table>
            </Col>
            <Col md={12}>
              <h6 className="subtitle">{keys[2]}</h6>
              <Tabs>
                {Object.keys(tabsData).map((key, i) => {
                  return (
                    <Tab eventKey={key} title={key}>
                      <Class data={tabsData[key]} path={path} />
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default Index;
