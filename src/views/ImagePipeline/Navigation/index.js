import React from 'react';
import { Row, Col, Container, Navbar, Nav, Image, NavDropdown } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";

class Index extends React.Component {
  render(){
    var { data } = this.props;
    return (
        <Navbar collapseOnSelect bg="dark" variant="dark" expand="lg">
          <Navbar.Brand href="#home" className="cwhite">Report</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="navigation-bar">
            <Nav className="justify-content-end" defaultActiveKey="#loading" variant="pills">
              <Nav.Item><Nav.Link href="#loading">Data Diagnostic</Nav.Link></Nav.Item>
              <Nav.Item><Nav.Link href="#modeling">Modeling Pipeline</Nav.Link></Nav.Item>
              <NavDropdown title="Model Explainability">
                <NavDropdown.Item eventKey="3.1" href="#exp-whitebox">White Box (Saliency Map)</NavDropdown.Item>
                <NavDropdown.Item eventKey="3.2" href="#exp-blackbox">Black Box (LIME)</NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="Attack Vectors">
                <NavDropdown.Item eventKey="4.1" href="#art-whitebox">White Box</NavDropdown.Item>
                <NavDropdown.Item eventKey="4.2" href="#art-blackbox">Black Box</NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="Metamorphic Testing" id="#metamorphic">
                <NavDropdown.Item eventKey="5.1" href="#meta-summary">Metamorphic Testing Summary</NavDropdown.Item>
                <NavDropdown.Item eventKey="5.2" href="#meta-whitebox">White Box</NavDropdown.Item>
                <NavDropdown.Item eventKey="5.3" href="#meta-blackbox">Black Box</NavDropdown.Item>
              </NavDropdown>
              <Nav.Item><Nav.Link href="#deepXplore">DeepXplore</Nav.Link></Nav.Item>
              <Nav.Item><Nav.Link href="#feedback_loop">Feedback Loop</Nav.Link></Nav.Item>
              <Nav.Item><Nav.Link href="#model_privacy">Model Privacy</Nav.Link></Nav.Item>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
    );
  }
}

export default Index;
