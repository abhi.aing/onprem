import React from 'react';
import { Row, Col, Container, Nav, Image, Table, ProgressBar } from 'react-bootstrap';
import "./index.css";
import { Doughnut, Bar } from 'react-chartjs-2';

class Index extends React.Component {
  render(){
    const { title, data } = this.props;
    const keys = Object.keys(data);
    var rowsTable1Html = Object.keys(data[keys[2]]).map((rowKey, i) => {
      return (
        <tr>
          <td>{rowKey}</td>
          {
            Object.keys(data[keys[2]][rowKey]).map((col, i) =>
            <td>{(data[keys[2]][rowKey][col]).join(", ")}</td>)
          }
        </tr>
      );
    });
    var rowsTable2Html = Object.keys(data[keys[4]]).map((rowKey, i) => {
      return (
        <tr>
          <td>{rowKey}</td>
          {
            Object.keys(data[keys[4]][rowKey]).map((col, i) =>
            <td>{(data[keys[4]][rowKey][col]).join(", ")}</td>)
          }
        </tr>
      );
    });
    var rowsTable1 = Object.keys(data[keys[2]]);
    var rowsTable2 = Object.keys(data[keys[4]]);
    return (
      <Row id="meta-summary">
        <Col>
          <h6 className="subtitle">{title}</h6>
          <br/><br />
          {keys[1]}- {data[keys[1]]}<br />
          {keys[0]} - {data[keys[0]]}<br />
          {keys[3]} : {data[keys[3]]}<br /><br />
          {

            (data[keys[2]][rowsTable1[0]]) == undefined ? null : (
              <>
                <h7 className="tablename">Corner Cases</h7>
                <Table striped bordered hover responsive size="lg" variant="dark">
                  <thead>
                    <tr>
                      <th>Classname</th>
                      {Object.keys(data[keys[2]][rowsTable1[0]]).map((rowKey, i) => <th>{rowKey}</th>)}
                    </tr>
                  </thead>
                  <tbody>
                    {rowsTable1Html}
                  </tbody>
                </Table>
              </>
            )
          }
          <br />
          {
            data[keys[4]][rowsTable2[0]] == undefined ? null : (
              <>
                <h7 className="tablename">Normal Cases</h7>
                <Table striped bordered hover responsive size="lg" variant="dark">
                  <thead>
                    <tr>
                      <th>Classname</th>
                      {Object.keys(data[keys[4]][rowsTable2[0]]).map((rowKey, i) => <th>{rowKey}</th>)}
                    </tr>
                  </thead>
                  <tbody>
                    {rowsTable2Html}
                  </tbody>
                </Table>
              </>
            )
          }
        </Col>
      </Row>
    );
  }
}

export default Index;
