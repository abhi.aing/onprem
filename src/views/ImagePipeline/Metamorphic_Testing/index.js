import React from 'react';
import { Row, Col, Container, Nav, Image } from 'react-bootstrap';
import "./index.css";
import Whitebox from './Whitebox';
import Blackbox from './Blackbox';
import Summary from './Summary';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    return (
      <Row id="metamorphic">
        <Col className="section">
          <h5 className="title">{title}</h5>
          <br />
          <h6>{keys[1]}{data[keys[1]]}</h6>
          <br />
          <ul>
            <li>
              <Summary title={keys[0]} data={data[keys[0]]} path={path} />
            </li>
            <br />
            <li>
              <Whitebox title={keys[2]} data={data[keys[2]]} path={path} />
            </li>
            <br />
            <li>
              <Blackbox title={keys[3]} data={data[keys[3]]} path={path} />
            </li>
          </ul>
        </Col>
      </Row>
    );
  }
}

export default Index;
