import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import "./index.css";
import Gallery from 'react-grid-gallery';;

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    var tableHtml = keys.map((key, i) => {
      if(i == 0 || i == 1) return null;
      return (
        <tr key={i}>
          <td>{key}</td>
          <td>{data[key].loss}</td>
          <td>{data[key].accuracy}</td>
        </tr>
      );
    });
    var IMAGES = [{
        src: process.env.PUBLIC_URL + path + data[keys[0]],
        thumbnail: process.env.PUBLIC_URL + path + data[keys[0]],
        thumbnailWidth: 450,
        thumbnailHeight: 200,
      },
      {
        src: process.env.PUBLIC_URL + path + data[keys[1]],
        thumbnail: process.env.PUBLIC_URL + path + data[keys[1]],
        thumbnailWidth: 450,
        thumbnailHeight: 200,
      }];
    return (
      <section>
        <Row>
          <Col className="section">
            <h6 className="subtitle">{title}</h6>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Gallery images={IMAGES} backdropClosesModal={true} />
          </Col>
        </Row>
        <Table striped bordered hover responsive size="lg" variant="dark">
          <thead>
            <tr>
              <th>Dataset</th>
              <th>Loss</th>
              <th>Accuracy</th>
            </tr>
          </thead>
          <tbody>
            {tableHtml}
          </tbody>
        </Table>
      </section>
    )
  }
}

export default Index;
