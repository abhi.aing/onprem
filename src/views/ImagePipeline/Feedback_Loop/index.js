import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";
import Before from './Before';
import Report from './Report';
import Prediction from './Prediction';

class Index extends React.Component {
  constructor(props){
    super(props);

  }

  componentDidMount(){

  }

  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    var rowsHtml = keys.map((key, i) => {
      return (
        <tr key={i}>
          <td>{key}</td>
          <td>{data[keys[2]]}</td>
        </tr>
      );
    });
    //<Report title={keys[1]} data={data[keys[1]]} />

    return (
      <section id="feedback_loop">
        <Row>
          <Col md={12}>
            <h5 className="title">{title}</h5>
          </Col>
        </Row>
        <Before title={keys[0]} data={data[keys[0]]} path={path} />
        <Report title={keys[1]} data={data[keys[1]]} path={path} />
        <Prediction title={keys[2]} data={data[keys[2]]} path={path} />
      </section>
    );
  }
}

export default Index;
