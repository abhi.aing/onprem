import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import "./index.css";
import { Gallery } from '../../../../components';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    return (
      <section>
        <Row>
          <Col className="section">
            <h6 className="subtitle">{title}</h6>
            <Gallery data={data} path={path} />
          </Col>
        </Row>
      </section>
    )
  }
}

export default Index;
