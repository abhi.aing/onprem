import React from 'react';
import { Row, Col, Container, Nav, Image } from 'react-bootstrap';
import "./index.css";
import Whitebox from './Whitebox';
import Blackbox from './Blackbox';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    return (
      <Row id="explainability">
        <Col className="section" >
          <h5 className="title">{title}</h5>
          <ul>
            <li><Whitebox title={keys[0]} data={data[keys[0]]} path={path} /></li>
            <br />
            <li><Blackbox title={keys[1]} data={data[keys[1]]} path={path} /></li>
          </ul>
        </Col>
      </Row>
    );
  }
}

export default Index;
