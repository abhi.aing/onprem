import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import "./index.css";
import { Gallery } from '../../../../components';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    return (
      <Row id="exp-whitebox">
        <Col className="section">
          <h6 className="subtitle">{title}</h6>
          <p>{data.one_liner}</p>
          <p>{data.layer_name}</p>
          <Gallery data={data} path={path} />
        </Col>
      </Row>
    );
  }
}

export default Index;
