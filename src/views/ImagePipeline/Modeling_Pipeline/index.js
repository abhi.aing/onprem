import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";
import Gallery from 'react-grid-gallery';;
//var TFile = require('../data/covid_mask/meta_info/model_summary.txt');

//console.log(TFile);

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    var tableHtml = '';
    const keys = Object.keys(data);
    var tableHtml = keys.map((key, i) => {
      if(i > 1) return null;
      return (
        <tr key={i}>
          <td>{key}</td>
          <td>{data[key].loss}</td>
          <td>{data[key].accuracy}</td>
        </tr>
      );
    });
    var IMAGES = [{
        src: path + data[keys[2]],
        thumbnail: path + data[keys[2]],
        thumbnailWidth: 450,
        thumbnailHeight: 200,
      },
      {
        src: path + data[keys[3]],
        thumbnail: path + data[keys[3]],
        thumbnailWidth: 450,
        thumbnailHeight: 200,
      }];
    return (
      <Row id="modeling">
        <Col className="section">
          <h5 className="title">{title}</h5>
          {data.model_summary_path}
          <Row>
            <Col md={12}>
              <Gallery images={IMAGES} backdropClosesModal={true} />
            </Col>
          </Row>
          <Table striped bordered hover responsive size="lg" variant="dark">
            <thead>
              <tr>
                <th>Dataset</th>
                <th>Loss</th>
                <th>Accuracy</th>
              </tr>
            </thead>
            <tbody>
              {tableHtml}
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  }
}

export default Index;
