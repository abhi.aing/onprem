import React from 'react';
import { Row, Col, Container, Navbar, Nav, Image, NavDropdown } from 'react-bootstrap';
import  { Images } from '../../themes';
import "./index.css";

import Loading_Data from './Loading_Data';
import Modeling_Pipeline from './Modeling_Pipeline';
import Model_Expainability from './Model_Explainability';
import Attack_Vectors from './Attack_Vectors';
import Feedback_Loop from './Feedback_Loop';
import DeepXplore from './DeepXplore';
import Metamorphic_Testing from './Metamorphic_Testing';
import Model_Privacy from './Model_Privacy';
import Dataset_Selector from './Dataset_Selector';
import Navigation from './Navigation';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleUp } from '@fortawesome/free-solid-svg-icons';
import ScrollToTop from "react-scroll-to-top";
//import { ReactComponent as MySVG } from "../../images/uparrow.svg";
import {
  Header, Footer
} from '../../components';

var paths = [
  'data/imagepipeline/Animal/Results/'
];

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      choice: 0,
      data: null
    }
  }

  componentWillMount(){
    this.setData(0);
  }

  setData = (choice) => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    fetch(paths[choice] + "json_metadata.json")
    .then((res) => res.json())
    .then((data) => {
      this.setState({
        data: data,
        choice: choice
      });
    });
  }

  pickChoice = (choice) => {
    this.setData(--choice);
  }

  /*
  <Dataset_Selector choice={this.pickChoice} />

  */
  render(){
    const { choice, data } = this.state;
    if(data == null) return null;
    var keys = Object.keys(data);
    var path = paths[choice];
    return (
      <>
        <Container className="main">
          <Header title="Report - Image Pipeline" />
          <Navigation />
          <Loading_Data title={keys[0]} data={data[keys[0]]} path={path} />
          <hr />
          <Modeling_Pipeline title={keys[1]} data={data[keys[1]]} path={path} />
          <hr />
          <Model_Expainability title={keys[2]} data={data[keys[2]]} path={path} />
          <hr />
          <Attack_Vectors title={keys[3]} data={data[keys[3]]} path={path} />
          <hr />
          <Metamorphic_Testing title={keys[4]} data={data[keys[4]]} path={path} />
          <hr />
          <DeepXplore title={keys[5]} data={data[keys[5]]} path={path} />
          <hr />
          <Feedback_Loop title={keys[6]} data={data[keys[6]]} path={path} />
          <hr />
          <Model_Privacy title={keys[7]} data={data[keys[7]]} path={path} />
          <Footer />
          <ScrollToTop smooth color="#A03C64" />
        </Container>
      </>
    )
  }
}

export default Index;
