import React from 'react';
import { Row, Col, Container, Nav, Image, Table, Tabs, Tab } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";
import Case from './Case';

class Index extends React.Component {
  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data["Transformation"]);
    const key = Object.keys(data)[1];
    return (
      <section id="deepXplore">
        <Row>
          <Col md={12}>
            <h5 className="title">{title}</h5>
          </Col>
        </Row>
        <br />
          <h6>{key}{data[key]}</h6>
        <br />
        <Tabs defaultActiveKey={keys[0]}>
          {keys.map((key, i) => {
            return (
              <Tab eventKey={key} title={key}>
                <Case data={data["Transformation"][keys[i]]} path={path} />
              </Tab>
            );
          })}
        </Tabs>
      </section>
    );
  }
}

export default Index;
