import React from 'react';
import { Row, Col, Container, Navbar, Nav, Image, NavDropdown, Dropdown, ButtonGroup, Button } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null
    };
  }
  
  choiceSelect = (event) => {
    const { choice } = this.props;
    choice(event.target.attributes.getNamedItem('data-key').value);
  }

  render(){
    return (
      <Row className="selector">
        <Col md={12} align="left">
          <Dropdown aria-label="Dataset Selector" onClick={e => this.setState({ selectedItem: e.target.innerText })}>
            <Dropdown.Toggle variant="secondary" id="dropdown-basic">
              {this.state.selectedItem?this.state.selectedItem:"California"}
            </Dropdown.Toggle>

            <Dropdown.Menu onClick={this.choiceSelect}>
              <Dropdown.Item data-key="1">California</Dropdown.Item>
              <Dropdown.Item data-key="2">Boston</Dropdown.Item>
              <Dropdown.Item data-key="3">MPG</Dropdown.Item>
              <Dropdown.Item data-key="4">Bike Sharing</Dropdown.Item>
              <Dropdown.Item data-key="5">Insurance</Dropdown.Item>
              <Dropdown.Item data-key="6">Real Estate</Dropdown.Item>
              <Dropdown.Item data-key="7">US Births</Dropdown.Item>
              <Dropdown.Item data-key="8">Used Cars</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Col>
      </Row>
    );
  }
}

export default Index;
