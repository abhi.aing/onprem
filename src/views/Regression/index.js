import React from 'react';
import { Row, Col, Container, Navbar, Nav, Image, NavDropdown } from 'react-bootstrap';
import  { Images } from '../../themes';
import "./index.css";
import Global_Visualization from './Global_Visualization';
import Local_Visualization from './Local_Visualization';
import Model_Paths from './Model_Paths';
import Data_Paths from './Data_Paths';
import Bias from './Bias';
import Gaminet from './Gaminet';
import Navigation from './Navigation';
import {
  Header,
  Footer
} from '../../components';
import Dataset_Selector from './Dataset_Selector';
import ScrollToTop from "react-scroll-to-top";
//import { ReactComponent as MySVG } from "../../images/uparrow.svg";
var paths = [
  'data/regression/results_bike_sharing/'
];

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      choice: 1,
      data: null
    }
  }

  componentWillMount(){
    this.setData(0);
  }

  setData = (choice) => {
    console.log(choice + paths[choice]);
    fetch(paths[choice] + "json_metadata.json")
    .then((res) => res.json())
    .then((data) => {
      this.setState({
        data: data,
        choice: choice
      });
    });
  }

  pickChoice = (choice) => {
    this.setData(--choice);
  }

/*
  <Dataset_Selector choice={this.pickChoice} />

*/
  render(){
    const { choice, data } = this.state;
    if(data == null) return null;
    var keys = Object.keys(data);
    var path = paths[choice];
    return (
      <Container className="main">
        <Header title="Structured Data Report" />
        <Navigation data={keys} />
        <Data_Paths title={keys[0]} data={data[keys[0]]} path={path} />
        <hr />
        <Global_Visualization title={keys[1]} data={data[keys[1]]} path={path} />
        <hr />
        <Local_Visualization title={keys[2]} data={data[keys[2]]} path={path} />
        <hr />
        <Bias title={keys[3]} data={data[keys[3]]} path={path} />
        <hr />
        <Gaminet title={keys[4]} data={data[keys[4]]} path={path} />
        <br />
        <Footer />
        <ScrollToTop smooth color="#A03C64" />
      </Container>
    )
  }
}

export default Index;
