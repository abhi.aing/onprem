import React from 'react';
import { Row, Col, Container, Nav, Image, Table } from 'react-bootstrap';
import  { Images } from '../../../themes';
import "./index.css";
import Attack from './Attack';

class Index extends React.Component {
  constructor(props){
    super(props);

  }

  componentDidMount(){

  }

  render(){
    const { title, data, path } = this.props;
    const keys = Object.keys(data);
    return (
      <section id="bias">
        <Row>
          <Col md={12}>
            <h5 className="title">{title}</h5>
          </Col>
        </Row>
        <ul>
          <li>
            <Row>
              <Col md={12}>
                <Attack title={keys[0]} data={data[keys[0]]} path={path} />
              </Col>
            </Row>
          </li>
        </ul>
      </section>
    );
  }
}

export default Index;
