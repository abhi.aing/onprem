import React, {useState} from 'react';
import { Row, Col, Container, Nav, Image, Form, Button } from 'react-bootstrap';
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link, Redirect, useHistory, useLocation } from 'react-router-dom';
import  { Images } from './themes';
import ImagePipeline from './views/ImagePipeline';
import Structured from './views/Structured';
import Regression from './views/Regression';
import TextPipeline from './views/TextPipeline';
import Config from './views/Config';
import Auth from './views/Auth';
import Login from './views/Auth/Login';
import Signup from './views/Auth/Signup';
import Forget from './views/Auth/Forget';
import Profile from './views/Auth/Profile';
import { Header } from './components';
import Parse from './components/ParseService';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      password: ""
    }
  }

  state = {
    loading: true
  };

  componentDidMount() {
    this.fakeRequest().then(() => {
      const el = document.querySelector(".loader-container");
      if (el) {
        el.remove();  // removing the spinner element
        this.setState({ loading: false }); // showing the app
      }
    });
  }

  fakeRequest = () => {
    return new Promise(resolve => setTimeout(() => resolve(), 500));
  };

  render() {
    if (this.state.loading) {
      return null; //app is not ready (fake request is in process)
    }
    return (
      <Router>
        <div className="App">
          <Switch>
            <Route exact path="/structured">
              <Structured />
            </Route>
            <Route exact path="/regression">
              <Regression />
            </Route>
            <Route exact path="/">
              <ImagePipeline />
            </Route>
            <Route exact path="/text">
              <TextPipeline />
            </Route>
            <Route exact path="/login">
              <LoginPage />
            </Route>
            <Route exact path="/config">
              <Config />
            </Route>
            <Route exact path="/auth">
              <Auth />
            </Route>
            <Route exact path="/auth/login">
              <Login />
            </Route>
            <Route exact path="/auth/signup">
              <Signup />
            </Route>
            <Route exact path="/auth/forget">
              <Forget />
            </Route>
            <Route exact path="/auth/profile">
              <Profile />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

const fakeAuth = {
  isAuthenticated: false,
  authenticate(cb) {
    const currentUser = Parse.User.current();
    if(currentUser){
      //Parse.User.logOut();
      console.log(currentUser);
      fakeAuth.isAuthenticated = true;
      setTimeout(cb, 100); // fake async
    }
  },
  signout(cb) {
    fakeAuth.isAuthenticated = false;
    setTimeout(cb, 100);
  }
};

function PrivateRoute({children, ...rest}){
  return (
    <Route
      {...rest}
      render={({ location }) => {
        return (
          fakeAuth.isAuthenticated ? (
            children
          ) : (
            <Redirect to = {{
              pathname: "/auth/login",
              state: { from : location }
            }}
            />
          )
        )
      }
      }
    />
  );
}

function LoginPage() {
  const [password, setPassword] = useState("");
  let history = useHistory();
  let location = useLocation();

  let { from } = location.state || { from: { pathname: "/" } };
  let login = () => {
    if(password == "demo123" || password == "pass123"){
      fakeAuth.authenticate(() => {
        history.replace(from);
      });
    }
  };

  return (
    <Container className="main">
      <Header title="Testaing Reports" />
      <br /><br /><br /><br />
      <Row>
        <Col md={{ span: 4, offset:4 }}>
          <Form.Group controlId="loginForm" onSubmit={login}>
            <Form.Label><h5>Enter Password to Access</h5></Form.Label>
            <Form.Control type="password"
              placeholder="Password"
              value={password}
              onChange={(event) => setPassword(event.target.value)} />
            <br />
            <Button variant="outline-secondary" type="submit" onClick={login} block>
              Log In
            </Button>
          </Form.Group>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
