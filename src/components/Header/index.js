import React, { useState } from 'react';
import { Row, Col, Container, Nav, Image, Dropdown } from 'react-bootstrap';
import  { Images } from '../../themes';
import "./index.css";
import Parse from '../ParseService';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      selectedItem: null,
      setSelectedItem: null,
      username: null
    };
  }

  static defaultProps = {
      showReportSelector: true
  }

  test = () => {
    let str = window.location.href, ps;
    let ip = str.indexOf("");
    let tp = str.indexOf("text");
    let rp = str.indexOf("regression");
    let sp = str.indexOf("structured");
    let lg = str.indexOf("login");
    let cg = str.indexOf("config");
    let au = str.indexOf("auth");
    if (ip !== -1)
      ps = "Image Pipeline"
    if (tp !== -1)
      ps = "Text Pipeline"
    if (rp !== -1)
      ps = "Regression Pipeline"
    if (sp !== -1)
      ps = "Structured Pipeline"
    if (lg !== -1 || cg !== -1 || au !== -1)
      ps = "Choose Pipeline"
    return (ps);
  }

  logout = (event) => {
    event.preventDefault();
    var currentUser = Parse.User.current();
    if(currentUser){
      Parse.User.logOut();
    }
  }

  componentDidMount() {
    var currentUser = Parse.User.current();
    if(currentUser){
      this.setState({
        username: currentUser.getUsername()
      });
    }
  }

  render() {
    const { selectedItem, setSelectedItem , username} = this.state;
    const { showReportSelector } = this.props;
    var reportSelector = null;
    if(showReportSelector){
      reportSelector = (<Col xs={6} md={8} align="right" className="header-text">
        <Dropdown>
          <Dropdown.Toggle variant="secondary" id="dropdown-basic">
            {selectedItem?selectedItem:this.test()}
          </Dropdown.Toggle>
          <Dropdown.Menu onClick={e => setSelectedItem(e.target.innerText)}>
            <Dropdown.Item href="/">Image Pipeline</Dropdown.Item>
            <Dropdown.Item href="/text">Text Pipeline</Dropdown.Item>
            <Dropdown.Item href="/regression">Regression Pipeline</Dropdown.Item>
            <Dropdown.Item href="/structured">Structured Pipeline</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Col>);
    } else {
      reportSelector = (<Col xs={8} md={8}>
      </Col>)
    }
    return (
      <>
        <Row className="App-header sticky">
          <Col xs={4} md={2}>
            <a href="/"><img src={Images.logo} className="App-logo" alt="logo" /></a>
          </Col>
          {reportSelector}
          <Col xs={2} md={2} align="right">
            <Dropdown>
              <Dropdown.Toggle variant="secondary" id="profile-menu" className="">
                {username}
              </Dropdown.Toggle>
              <Dropdown.Menu onClick={e => this.setState({setSelectedItem: e.target.innerText})}>
                <Dropdown.Item href="/">Profile</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="/" onClick={this.logout}>Logout</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
        </Row>
        <hr />
      </>
    )
  }
}

export default Index;
