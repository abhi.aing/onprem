import React from 'react';
import { Row, Col, Container, Nav, Image } from 'react-bootstrap';
import  { Images } from '../../themes';
import "./index.css";
import ImageGallery from 'react-image-gallery';

class Index extends React.Component {
  constructor(props){
    super(props);
  }

  fetchImages = (data, path) => {
    var imagesList = [];
    var images = data.img_path;
    for(var i=0;i< images.length;i++){
      imagesList.push({
        original: path + images[i],
        thumbnail: path + images[i]
      });
    }
    return imagesList;
  }

  render(){
    const { data, path } = this.props;
    var imagesList = this.fetchImages(data, path);
    if(imagesList.length == 0) return null;
    return (
      <div>
        <ImageGallery
          items={imagesList}
          lazyLoad
          thumbnailPosition="left"
          showPlayButton= {false}
          useBrowserFullscreen={false}
        />
      </div>
    )
  }
}

export default Index;
