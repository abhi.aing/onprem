import React from 'react';
import { Row, Col, Container, Nav, Image } from 'react-bootstrap';
import  { Images } from '../../themes';
import "./index.css";

class Index extends React.Component {
  render(){
    return (
      <Row className="footer">
        <Col md={12}>
          All Rights Reserved © TestAIng
        </Col>
      </Row>
    );
  }
}

export default Index;
