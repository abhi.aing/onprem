import Header from './Header';
import Gallery from './Gallery';
import Footer from './Footer';

export {
  Header,
  Gallery,
  Footer
};
